=======
crackie
=======

A simple, silly library to generate combinations.

I wrote this when I forgot part of a password, and decided to try run ~500,000
combinations on the parts I remembered. It worked! Also I decided to publish it
because I had never created a Python package before, so this would learn me
something.

Contents
========

.. toctree::
   :maxdepth: 2

   License <license>
   Authors <authors>
   Changelog <changelog>
   Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
