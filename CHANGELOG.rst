=========
Changelog
=========

Version 1.0.1
=============

- (2022-11-08) Repo moved to GitLab.
- (2022-11-08) Trunk branch is now `main` (was `master`).

Version 1.0
===========

- First version.
